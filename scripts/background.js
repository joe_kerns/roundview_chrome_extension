chrome.runtime.onInstalled.addListener( function(details) {
    if(details.reason == "install") {
        chrome.tabs.create({url: "https://roundview.co/chrome_extension_installed"});
    }
});
// var environment = "development"
var environment = "production";

if(environment == "development") {
    var baseUrl = "http://localhost:3000";
} else {
    var baseUrl = "https://roundview.co";    
};


var saveUrlEndpoint = baseUrl + "/reading_logs/new";

var launchLogin = function() {
	var loginUrl = baseUrl + "/users/sign_in?extension=chrome"
	width = 430
    height = 660
	
    // Create a popup window 
	chrome.windows.create({
                url: loginUrl,
                type: "popup",
                width: width,
                height: height,
                left: Math.floor(screen.width / 2 - (width + 1) / 2),
                top: Math.floor(screen.height / 2 - height / 2)
            })

    // Create a new tab 
    // chrome.tabs.create({
    //             url: loginUrl
    //         })
}

var sendArticle = function(url, title) {
		data = {
			"article": {
				"url": url,
                "title": title,
                "source": "chrome"
			}
		}
		$.ajax({
            url: saveUrlEndpoint,
            type: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(data),
            dataType: "json", 
            success: function(response, status) {
                sendStatusMessage({message: "article saved"}); 
            },
            error: function(response, status, error) {
                if(status == "parsererror") {
                    sendStatusMessage({message: "login required"});
                    chrome.storage.local.set({'url_to_save': url, "title_to_save": title})
                    launchLogin();
                } else {
                    sendStatusMessage({message: "error saving article"});    
                }
                
            }
        })
}

var sendStatusMessage = function(message) {
    chrome.runtime.sendMessage(message); 
}

// chrome.browserAction.onClicked.addListener(function() {});

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.message == "send") {
            sendArticle(request.url, request.title);
            sendResponse({content: "article sent"});    
        }
});

chrome.tabs.onUpdated.addListener(
    function(tabId, changeInfo, tab) {
        if(tab.url.indexOf("roundview.co/extension_login") > -1 ) {
            setTimeout(function() { chrome.tabs.remove(tabId)}, 2000);
            chrome.storage.local.get(["url_to_save", "title_to_save"], function(result) {
                sendArticle(result.url_to_save, result.title_to_save);
            });

        }        
    });
