
var savedText = "<h3 class='grey-text'> Great job! Another article read! </h3> <p class='lead'><a target='_blank' href='https://roundview.co'>Check out your progress</a></p>";
var errorSavingText = "<h3 class='grey-text'>Roundview couldn't save this article. Please try again! </h3>"

var showMessage = function(text) {

	$("#result-block").append(text);
	$(".loading-block").slideUp(400, "linear", function() {
		$("#result-block").slideDown("slow", "linear", function() {
			if($("html").height() > 200) {
				$("body").height(120);
				$("html").height(120);
			}
		});
	});
	

};



var sendArticleSaveMessage = function() {
	chrome.tabs.getSelected(null, function(tab) { 
		chrome.runtime.sendMessage({message: "send", url: tab.url, title: tab.title}, function(response) {
		}); 
	});
};

chrome.runtime.onMessage.addListener(
	  	function(request, sender, sendResponse) {
	  		if(request.message == "article saved") {
	  			showMessage(savedText);
	  			setTimeout(function() { window.close()}, 4000);
	  		}
			else if (request.message == "error saving article") {
	  			showMessage(errorSavingText);
				setTimeout(function() { window.close()}, 4000);
			}
	      sendResponse({farewell: "article sending message received"});

	});	

$( window ).load(function() {
// $(document).ready(function() {
	$('.content-block').slideDown("slow", "linear");
	sendArticleSaveMessage();
});

